import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";



import LoginPage from "./components/p_login_page"
import RegisterPage from "./components/p_register_page"
import DashboardPage from "./components/p_dashboard_page"

// import logo from './logo.svg';

import './App.css';

const NoMatchPage = () => {
	return (
		<h3>404 - Not found</h3>
	);
};

function App() {
  return (
	<div className="App">

		<Router>
			<Switch>
				<Route exact path="/dashboard">
					<DashboardPage/>
				</Route>

				<Route exact path="/register">
					<RegisterPage/>
				</Route>

				<Route exact path="/">
					<LoginPage/>
				</Route>

				<Route component={NoMatchPage} />

			</Switch>

		</Router>

	</div>
  );
}

export default App;
