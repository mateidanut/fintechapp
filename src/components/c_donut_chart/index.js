import React from 'react';
import ReactApexChart from 'react-apexcharts'

class DonutChart extends React.Component {
      
    constructor(props) {
      super(props);

      this.state = {
        options: {
          dataLabels: {
            enabled: false
          },

          responsive: [{
            breakpoint: 480,
            options: {
              chart: {
                width: 200
              },
              legend: {
                show: false
              }
            }
          }],

          legend: {
              position: 'right',
              offsetY: 0,
              height: 230,
          }
        },
        
        series: [44, 55, 13, 33],
      }
    }

    render() {
        return (
            <div id="chart">
                <ReactApexChart options={this.state.options} series={this.state.series} type="donut" width="380" />
            </div>
        );
    }
}

export default DonutChart