import React from 'react';

import {withRouter} from 'react-router';

import {Link} from "react-router-dom";

import "./header_dashboard.css"

class HeaderDashboard extends React.Component {

    render() {
        return (
            <div className="header-dashboard-container">

                <div className="left-buttons">
                    <p> Dashboard </p>
                </div>

                <div className="right-buttons">
                    <Link to={{
                        pathname: "/",
                        state: {userToken: ""}
                    }}> 
                        Logout
                    </Link>
                </div>
            </div>
        );
    }

}

export default withRouter(HeaderDashboard)