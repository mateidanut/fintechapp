import React from 'react';

import { sha256 } from 'js-sha256';
// import { useHistory } from "react-router-dom";
// import { browserHistory } from 'react-router'

import {withRouter} from 'react-router';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

const style = {
    margin: 15,
};

// const history = useHistory()

class Login extends React.Component {

    constructor(props){
        super(props);
        this.state={
            username:'',
            password:''
        }
    }


    async handleClick(event) {

        var apiBaseUrl = "https://s5gnlmdvs7.execute-api.eu-west-1.amazonaws.com/Beta/create-user/";

        var payload={
            "user": this.state.username,
            "password": sha256(this.state.password)
        }

        var server_res = await fetch(apiBaseUrl, {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(payload)
        })
        .then(res => res.json())

        console.log(server_res)

        if (server_res.statusCode !== '200') {
            console.log("Received token " + server_res.body)
            this.props.history.push({
                pathname: '/dashboard',
                state: {userToken: server_res.body}
            });
        } else {
            console.log("Eroare")
        }

    }


    render() {
        return (
          <div>
            <MuiThemeProvider>
                <div>
                    <AppBar
                        title="Login"
                    />
                    <TextField
                        hintText="Enter your Username"
                        floatingLabelText="Username"
                        onChange = {(event,newValue) => this.setState({username:newValue})}
                    />

                    <br/>

                    <TextField
                        type="password"
                        hintText="Enter your Password"
                        floatingLabelText="Password"
                        onChange = {(event,newValue) => this.setState({password:newValue})}
                    />

                    <br/>

                    <RaisedButton label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)}/>
                </div>
             </MuiThemeProvider>
          </div>
        );
    }
}

export default withRouter(Login)