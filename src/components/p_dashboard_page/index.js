import React from 'react';

import {withRouter} from 'react-router';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import HeaderDashboard from "../c_header_dashboard"

import DonutChart from "../c_donut_chart"


import "./dashboard.css"


class DashboardPage extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            username: 'GENERIC USER',
            balance: 0
        }
    }

    componentDidMount() {
        
    }

    async handleClick(event) {
        
        const userToken = this.props.location.state.userToken

        // console.log(userToken)

        if (userToken) {

            var apiBaseUrl = "https://s5gnlmdvs7.execute-api.eu-west-1.amazonaws.com/Beta/api/sold";

            var payload={
                "token": userToken
            }

            var server_res = await fetch(apiBaseUrl, {
                method: 'POST',
                mode: 'cors',
                body: JSON.stringify(payload)
            })
            .then(res => res.json())

            console.log(server_res)

            // if (server_res.statusCode !== '200') {
            //     console.log("Received response: " + server_res.body)
            // } else {
            //     console.log("Eroare")
            // }

        }

    }

    render() {
        return (
            <MuiThemeProvider>
                <div className="dashboard-layout">
                    <HeaderDashboard/>

                    <div className="dashboard-content-wrapper">

                        <div className="dashboard-content">

                            <p> {"Welcome " + this.state.username} </p>
                            <p> {"Current balance: " + this.state.balance} </p>

                            <p onClick={(event) => this.handleClick(event)}> Get money </p>
                            <p onClick={() => console.log(this.props.location.state.userToken)}> Token Buton </p>

                            
                            <DonutChart/>

                        </div>

                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default withRouter(DashboardPage)