import React from 'react';

import {withRouter} from 'react-router';

import {Link} from "react-router-dom";

import Login from "../c_login"

class LoginPage extends React.Component {

    render() {
        return (
            <div>
                <Login/>

                <Link to="/register"> Not registered yet? Create an account now! </Link>

                <p onClick={() => console.log(this.props.location.state.userToken)}> Buton </p>
            </div>
        );
    }
}

export default withRouter(LoginPage)