import React from 'react';

// import { sha256 } from 'js-sha256';
import {withRouter} from 'react-router';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

const style = {
    margin: 15,
};

// const history = useHistory()

class RegisterPage extends React.Component {

    constructor(props){
        super(props);
        this.state={
            username: "",
            firstname: "",
            lastname: "",
            password: "",
            email: "",
            telnr: "",
        }
    }


    async handleClick(event) {

        // var apiBaseUrl = "https://s5gnlmdvs7.execute-api.eu-west-1.amazonaws.com/Beta/create-user/";

        // var payload={
        //     "user": this.state.username,
        //     "password": sha256(this.state.password)
        // }

        // var server_res = await fetch(apiBaseUrl, {
        //     method: 'POST',
        //     mode: 'cors',
        //     body: JSON.stringify(payload)
        // })
        // .then(res => res.json())

        // console.log(server_res)

        // if (server_res.statusCode !== '200') {
        //     console.log("Received token " + server_res.body)
        //     this.props.history.push('/dashboard');
        // } else {
        //     console.log("Eroare")
        // }

    }


    render() {
        return (
          <div>
            <MuiThemeProvider>
                <div>
                    <AppBar
                        title="Registration"
                    />
                    <TextField
                        hintText="Enter your Username"
                        floatingLabelText="Username"
                        onChange = {(event, newValue) => this.setState({username: newValue})}
                    />
                    <br/>

                    <TextField
                        hintText="Enter your first name"
                        floatingLabelText="First name"
                        onChange = {(event, newValue) => this.setState({firstname: newValue})}
                    />
                    <br/>

                    <TextField
                        hintText="Enter your last name"
                        floatingLabelText="Last name"
                        onChange = {(event, newValue) => this.setState({lastname: newValue})}
                    />
                    <br/>

                    <TextField
                        hintText="Enter your email address"
                        floatingLabelText="Email address"
                        onChange = {(event, newValue) => this.setState({email: newValue})}
                    />
                    <br/>

                    <TextField
                        hintText="Enter your telephone number"
                        floatingLabelText="Telephone number"
                        onChange = {(event, newValue) => this.setState({telnr: newValue})}
                    />
                    <br/>

                    <TextField
                        type="password"
                        hintText="Enter your Password"
                        floatingLabelText="Password"
                        onChange = {(event, newValue) => this.setState({password: newValue})}
                    />
                    <br/>

                    <RaisedButton label="Register" primary={true} style={style} onClick={(event) => console.log(event)}/>
                </div>
             </MuiThemeProvider>
          </div>
        );
    }
}

export default withRouter(RegisterPage)